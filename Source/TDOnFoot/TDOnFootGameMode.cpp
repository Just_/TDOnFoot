// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDOnFootGameMode.h"
#include "TDOnFootPlayerController.h"
#include "TDOnFootCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDOnFootGameMode::ATDOnFootGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDOnFootPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}