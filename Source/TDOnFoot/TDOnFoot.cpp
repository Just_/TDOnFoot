// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDOnFoot.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDOnFoot, "TDOnFoot" );

DEFINE_LOG_CATEGORY(LogTDOnFoot)
 