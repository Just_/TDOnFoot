// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDOnFootGameMode.generated.h"

UCLASS(minimalapi)
class ATDOnFootGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDOnFootGameMode();
};



